# Perl6 helper-scripts for SVG paths mirror-transposing along X-axis

## Overview

Script for mirror transposing X-axis coordinates from SVG paths.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html). 
