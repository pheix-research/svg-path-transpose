#!/usr/bin/env perl6

use v6.c;

my @tag    = ( "<path id=\"p%id%\" class=\"%class%\" filter=\"url(#f1)\" d=\"", "\"/>" );
my $mirror = 299;
my $cnt    = 77;
my $fnin   = "./data.html";
my $fnout  = !@*ARGS[0] ?? "/home/kostas/git/chaoslab/brain-templ/index.html" !! @*ARGS[0];

( "Settings:\nIN: "~ $fnin ~"\nOUT: "~ $fnout ~"\n" ).say;

my @paths;
for $fnin.IO.slurp.lines.reverse -> $l {
    if ( $l ~~ / 'd="' (<[0..9LM\s,]>+) ' Z"' /) {
        my UInt $i    = 0;
        my Str $class = "polypath polyset-top1";
        my @data;
        for $0.split(/\s/) -> $d {
            if ( $d ~~ / (<[0..9]>+) ',' (<[0..9]>+) / ) {
                # say 'x=' ~ $0 ~ ', y=' ~ $1;
                my Int $x     = $0.Int;
                my Int $y     = $1.Int;
                my Int $new_x = ( $mirror - $x ) + $mirror;
                if ( $i == 0 ) {
                    @data.push( "M" ~ $new_x ~ "," ~ $y );
                } else {
                    @data.push( "L" ~ $new_x ~ "," ~ $y );
                }
                $i++;
            }
        }
        my $tag_s = @tag[0];
        $tag_s    ~~ s/'%id%'/$cnt/;
        if ( $l ~~ / 'class="' (<[a..z0..9\-\s]>+) '"' /) {
            $class = $0.Str;
        }
        $tag_s    ~~ s/'%class%'/$class/;
        @paths.push( $tag_s ~ @data.join(" ") ~ " Z" ~ @tag[1] );
        $cnt++;
    }
}

if ( ($fnout ~ ".tmpl").IO.e ) {
    my $content = @paths.join("\n");
    my $fn_cnt = ( $fnout ~ ".tmpl" ).IO.slurp;
    $fn_cnt ~~ s/'%brain2%'/$content/;
    my $out = open $fnout, :w;
    $out.say($fn_cnt);
    $out.close;
    "DONE!".say;
} else {
    ( "***ERR: Result is not saved - no template <" ~ $fnout ~".tmpl> found!" ).say;
}
