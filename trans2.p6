#!/usr/bin/env perl6

use v6.c;

my $cnt    = 1;
my $json_b = "\{ \"id\": \"p%id%\", \"name\": \"poly-%namenum%\", \"class\": \"%class%\", \"wave\": \"\", \"data\": [ %data% ] \}";
my $fnin   = "./data2.html";
my $fnout  = !@*ARGS[0] ?? "/home/kostas/git/pheix-research/svg-path-transpose/polygons.json" !! @*ARGS[0];

( "Settings:\nIN: "~ $fnin ~"\nOUT: "~ $fnout ~"\n" ).say;
my @json_items;
for $fnin.IO.slurp.lines -> $l {
    if ( $l ~~ / 'd="' (<[0..9LM\s,]>+) ' Z"' /) {
        my UInt $i    = 0;
        my Str $class = "polypath polyset-top1";
        my @data;
        for $0.split(/\s/) -> $d {
            if ( $d ~~ / (<[0..9]>+) ',' (<[0..9]>+) / ) {
                # say 'x=' ~ $0 ~ ', y=' ~ $1;
                my Int $x     = $0.Int;
                my Int $y     = $1.Int;
                if ( $i == 0 ) {
                    @data.push( "\{ \"t\" : \"M\", \"x\": " ~ $x ~ ", \"y\": " ~ $y ~ "\}" );
                } else {
                    @data.push( "\{ \"t\" : \"L\", \"x\": " ~ $x ~ ", \"y\": " ~ $y ~ "\}" );
                }
                $i++;
            }
        }
        my $tag_s = $json_b;
        my $cnt_f = "%03d".sprintf($cnt);
        my $dat_f = @data.join(',');
        $tag_s    ~~ s/'%id%'/$cnt/;
        $tag_s    ~~ s/'%namenum%'/$cnt_f/;
        if ( $l ~~ / 'class="' (<[a..z0..9\-\s]>+) '"' /) {
            $class = $0.Str;
        }
        $tag_s    ~~ s/'%class%'/$class/;
        $tag_s    ~~ s/'%data%'/$dat_f/;
        @json_items.push( $tag_s );
        $cnt++;
    }
}


my $content = "var polygons = \[\n" ~ @json_items.join(",\n") ~ "\n\]";
my $out = open $fnout, :w;
$out.say($content);
$out.close;
"DONE!".say;
